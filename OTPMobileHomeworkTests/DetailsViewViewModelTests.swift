//
//  DetailsViewViewModelTests.swift
//  OTPMobileHomeworkTests
//
//  Created by Levente Szilágyi on 2019. 10. 28..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import XCTest
@testable import OTPMobileHomework

class DetailsViewViewModelTests: XCTestCase {

    var detailsModel: DetailsModel?
    var detailsViewModel: DetailsViewViewModel?
    
    override func setUp() {
        
        let apiKey = "65803e8f6e4a3982200621cad356be51"
        
        var photo: Photo?
        
        do {
            photo = try JSONDecoder().decode(Photo.self, from: photoResponse)
        } catch {
            
        }
        
        guard let image = photo else {
            return
        }
        
        detailsModel = DetailsModel.init(withPhoto: image, withApiKey: apiKey)
        let remoteService = RemoteServiceImpl()
        
        if detailsModel != nil {
            detailsViewModel = DetailsViewViewModel.init(withModel: detailsModel!, withRemoteService: remoteService)
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testForImageDecodeSuccess() {
        
        XCTAssertTrue(detailsModel?.photo?.id == "48972746841")
    }
    
    func testImageDetailSearch() {
        
        let expectation = XCTestExpectation(description: "Download Image Details")
        
        detailsViewModel?.searchForImageDetails {
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGetImageTitle() {
        
        if let detailsViewModel = detailsViewModel {
        
            XCTAssertEqual(detailsViewModel.getImageTitle(), "Howdy Hank!")
        }
    }
    
    func testGetImageOwner() {
        
        if let detailsViewModel = detailsViewModel {
        
            XCTAssertEqual(detailsViewModel.getImageOwner(), "150379803@N05")
        }
    }
    
    func testGetImageOwnerAfterDetails() {
        
        let expectation = XCTestExpectation(description: "Download Image Details")
        
        detailsViewModel?.searchForImageDetails {
            XCTAssertEqual(self.detailsViewModel?.getImageOwner(), "Glass Horse 2017")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGetURLStringForImage() {
        
        XCTAssertEqual(detailsViewModel?.getURLStringForImage(), "https://farm66.staticflickr.com/65535/48972746841_f6447d6765_b.jpg")
    }
    
    func testGetNumberOfDetailsWithoutDetails() {
        
        XCTAssertEqual(detailsViewModel?.getNumberOfDetails(), 0)
    }
    
    func testGetNumberOfDetailsWithDetails() {
        
        let expectation = XCTestExpectation(description: "Download Image Details")
        
        detailsViewModel?.searchForImageDetails {
            XCTAssertEqual(self.detailsViewModel?.getNumberOfDetails(), 5)
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5.0)
    }
    
    func testGetDetail() {
        
        let expectation = XCTestExpectation(description: "Download Image Details")
        
        detailsViewModel?.searchForImageDetails {
            XCTAssertTrue(self.detailsViewModel?.getDetail(at: 0) != nil)
            expectation.fulfill()
        }

        wait(for: [expectation], timeout: 5.0)
    }
}

private let photoResponse = Data("""
{
            "id": "48972746841",
            "owner": "150379803@N05",
            "secret": "f6447d6765",
            "server": "65535",
            "farm": 66,
            "title": "Howdy Hank!",
            "ispublic": 1,
            "isfriend": 0,
            "isfamily": 0
}
""".utf8)
