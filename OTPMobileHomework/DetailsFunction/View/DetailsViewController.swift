//
//  DetailsViewController.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 27..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation
import UIKit

class DetailsViewController: UIViewController {

    // MARK: - Public Properties
    
    // MARK: - Private Properties
    private var viewModel: DetailsViewViewModel?

    private let viewTitle = "Image Details"
    private var detailsCellIdentifier = "imageDetailsCell"
    
    private var imageViewsOriginalTransform: CGAffineTransform? = nil
    private var initialCenter = CGPoint()
    // MARK: Layout Elements
    
    private let activityView = UIActivityIndicatorView(style: .medium)
    
    private lazy var imageView: UIImageView = {
        
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .clear
        
        imageView.isUserInteractionEnabled = true
        let pinch = UIPinchGestureRecognizer(target: self, action: #selector(handleZoom))
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
        
        pan.minimumNumberOfTouches = 2
        pan.maximumNumberOfTouches = 2
        
        pan.delegate = self
        pinch.delegate = self
        
        imageView.addGestureRecognizer(pinch)
        imageView.addGestureRecognizer(pan)
        
        return imageView
    }()
    
    private lazy var separatorView: UIView = {
        
        let separatorView = UIView()
        separatorView.backgroundColor = .systemBackground
        
        return separatorView
    }()
    
    private lazy var fullScreenView: UIView = {
        
        let fullScreenView = UIView()
        fullScreenView.backgroundColor = .systemBackground
        fullScreenView.alpha = 0
        
        return fullScreenView
    }()
    
    private lazy var imageDetailsTableView: UITableView = {
        
        let detailsTableView = UITableView()
        detailsTableView.register(ImageDetailsViewCell.self, forCellReuseIdentifier: detailsCellIdentifier)
        
        detailsTableView.backgroundColor = .systemGray6
        return detailsTableView
    }()

    // MARK: - Public Methods
    // MARK: Initializers
    
    init(withViewModel detailsViewViewModel: DetailsViewViewModel) {
        
        super.init(nibName: nil, bundle: nil)
        
        viewModel = detailsViewViewModel
        
        imageDetailsTableView.dataSource = self
    }
       
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    // MARK: Lifecycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setupViewAppearance()
        
        layoutComponents()
        
        setupData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        resetImage()
    }
    
    // MARK: - Private Methods
    // MARK: Layouting Methods
    
    private func layoutComponents() {
        
        view.addSubview(fullScreenView)
        layoutFullScreenView()
        
        view.addSubview(imageView)
        layoutImageView()
        
        view.addSubview(separatorView)
        layoutSeparatorView()
        
        view.addSubview(imageDetailsTableView)
        layoutImageDetailsTableView()
        
        view.addSubview(activityView)
        layoutActivityView()
        
        view.bringSubviewToFront(fullScreenView)
        view.bringSubviewToFront(imageView)
    }
    
    private func layoutActivityView() {
        activityView.anchor(top: imageView.bottomAnchor,
                            trailing: view.trailingAnchor,
                            bottom: view.bottomAnchor,
                            leading: view.leadingAnchor)
    }
    
    private func layoutFullScreenView() {
        fullScreenView.anchor(top: view.topAnchor,
                              trailing: view.trailingAnchor,
                              bottom: view.bottomAnchor,
                              leading: view.leadingAnchor)
    }
    
    private func layoutImageDetailsTableView() {
        
        imageDetailsTableView.anchor(top: separatorView.bottomAnchor,
                                     trailing: view.safeAreaLayoutGuide.trailingAnchor,
                                     bottom: view.safeAreaLayoutGuide.bottomAnchor,
                                     leading: view.safeAreaLayoutGuide.leadingAnchor)
    }
    
    private func layoutSeparatorView() {
        
        separatorView.anchor(top: imageView.bottomAnchor,
                             trailing: view.safeAreaLayoutGuide.trailingAnchor,
                             bottom: nil,
                             leading: view.safeAreaLayoutGuide.leadingAnchor)
        
        separatorView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }
    
    private func layoutImageView() {
        
        imageView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                         trailing: view.safeAreaLayoutGuide.trailingAnchor,
                         bottom: nil,
                         leading: view.safeAreaLayoutGuide.leadingAnchor)
        
        imageView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 1/2).isActive = true
    }
    
    // MARK: Setup Methods
    private func setupViewAppearance() {
        
        navigationController?.navigationBar.prefersLargeTitles = false
        
        view.backgroundColor = .systemGray6
        title = viewTitle
        
        imageDetailsTableView.separatorColor = .clear
    }
    
    private func setupData() {
        
        guard let viewModel = viewModel else {
            return
        }
        
        let url = URL(string: viewModel.getURLStringForImage())
        imageView.kf.setImage(with: url)
        
        activityView.startAnimating()
        viewModel.searchForImageDetails {
            
            DispatchQueue.main.async { [unowned self] in
                self.title = viewModel.getImageTitle()
                self.imageDetailsTableView.reloadData()
                self.activityView.stopAnimating()
            }
        }
    }

    private func resetImage() {
        if let originalTransform = imageViewsOriginalTransform {
            imageView.transform = originalTransform
            imageView.center = initialCenter
        }
    }
}

// MARK: - Extensions

extension DetailsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let viewModel = viewModel else {
            return 0
        }
        
        return viewModel.getNumberOfDetails()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let viewModel = viewModel else {
            return UITableViewCell()
        }
        
        let cell = imageDetailsTableView.dequeueReusableCell(withIdentifier: detailsCellIdentifier, for: indexPath) as! ImageDetailsViewCell
        
        cell.showDetails(withModel: viewModel.getDetail(at: indexPath.row))
        cell.selectionStyle = .none
        return cell
    }
}

extension DetailsViewController: UIGestureRecognizerDelegate {
  
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @objc func handleZoom(_ gesture: UIPinchGestureRecognizer) {
        switch gesture.state {
        case .began, .changed:
            
            if gesture.scale >= 1 {

                let scale = gesture.scale
                gesture.view!.transform = CGAffineTransform(scaleX: scale, y: scale)
            }
          
            UIView.animate(withDuration: 0.2) {
                self.fullScreenView.alpha = 0.8
            }
            break;
        default:
            
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
              gesture.view!.transform = .identity
            }) { _ in
                
              UIView.animate(withDuration: 0.2) {
                self.fullScreenView.alpha = 0
              }
            }
        }
    }
    
    @objc func handlePan(_ gesture: UIPanGestureRecognizer) {
        
        switch gesture.state {
        case .began:
            
            initialCenter = imageView.center
            print(initialCenter)
            
        case .changed:

            let translation = gesture.translation(in: imageView)
            
            gesture.view!.center = CGPoint(x: imageView.center.x + translation.x*2.5, y: imageView.center.y + translation.y*2.5)
            gesture.setTranslation(.zero, in: imageView)

            UIView.animate(withDuration: 0.2) {
                self.fullScreenView.alpha = 0.8
            }
            break
        default:
            
            UIView.animate(withDuration: 0.25, delay: 0, options: .curveEaseInOut, animations: {
                gesture.view!.center = self.view.center
                gesture.setTranslation(.zero, in: self.view)
                self.imageView.center = self.initialCenter
                
            }) { _ in
                
              UIView.animate(withDuration: 0.2) {
                self.fullScreenView.alpha = 0
              }
            }
            break
        }
    }
}
