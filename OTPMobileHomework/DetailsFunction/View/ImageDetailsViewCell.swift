//
//  ImageDetailsViewCell.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 27..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation
import UIKit

class ImageDetailsViewCell: UITableViewCell {
    
    lazy var iconImageView: UIImageView = {
        let iconImageView = UIImageView()
        iconImageView.contentMode = .scaleAspectFit
        return iconImageView
    }()
    
    lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        
        titleLabel.textColor = .systemGray
        titleLabel.numberOfLines = 0
        return titleLabel
    }()
    
    lazy var detailLabel: UILabel = {
        let detailLabel = UILabel()
        
        titleLabel.textColor = .systemGray2
        detailLabel.numberOfLines = 0
        return detailLabel
    }()
    
    func showDetails(withModel model: ImageDetailsTableViewCellModel) {
        
        if let iconName = model.iconName {
            let config = UIImage.SymbolConfiguration(pointSize: 25, weight: .black, scale: .medium)
            if let image = UIImage(systemName: iconName , withConfiguration: config) {
                iconImageView.image = image.withTintColor(.systemGray, renderingMode: .alwaysOriginal)
            }
        }
        
        titleLabel.text = model.title
        detailLabel.text = model.text
        
        layoutComponents()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .systemGray6
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func layoutComponents() {
        
        addSubview(iconImageView)
        layoutIconImageView()
        
        addSubview(titleLabel)
        layoutTitleLabel()
        
        addSubview(detailLabel)
        layoutDetailLabel()
    }
    
    private func layoutDetailLabel() {
        
        detailLabel.anchor(top: titleLabel.bottomAnchor,
                           trailing: trailingAnchor,
                           bottom: bottomAnchor,
                           leading: titleLabel.leadingAnchor,
                           padding: UIEdgeInsets(top: 0, left: 0, bottom: 12, right: 10))
    }
    
    private func layoutTitleLabel() {
        
        titleLabel.anchor(top: iconImageView.topAnchor,
                          trailing: trailingAnchor,
                          bottom: nil,
                          leading: iconImageView.trailingAnchor,
                          padding: UIEdgeInsets(top: 5, left: 16, bottom: 0, right: 10))
    }
    
    private func layoutIconImageView() {
        
        iconImageView.anchor(top: topAnchor,
                             trailing: nil,
                             bottom: nil,
                             leading: leadingAnchor,
                             padding: UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 0))
        
        iconImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        iconImageView.widthAnchor.constraint(equalToConstant: 16).isActive = true
    }
}
