//
//  DetailsViewModel.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 27..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class DetailsViewViewModel {
    
    // MARK: - Public Properties
    
    // MARK: - Private Properties
    private var model: DetailsModel
    private var remoteService: RemoteService
    
    // MARK: - Public Methods
    // MARK: Initializers
    init(withModel newModel: DetailsModel, withRemoteService newRemoteService: RemoteService) {
           
        model = newModel
        remoteService = newRemoteService
    }
    
    // MARK: Other Public Methods
    func searchForImageDetails(completion: @escaping () -> Void) {
        
        guard let photo = model.photo else {
            return
        }
        
        let newUrl = String(format: model.baseImageDetailsURLFormat, model.apiKey, photo.id, photo.secret)
            
            print(newUrl)
            remoteService.fetchObjects(urlString: newUrl) {[unowned self] (object: PhotoDetailsResponse) in

                self.model.photo = object.photo
                self.createDetailObjects()
                completion()
            }
    }
    
    func getURLStringForImage() -> String {

        guard let photo = model.photo else {
            return ""
        }
        
        //https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
        let newUrl = String(format: model.baseImageURLFormat, "\(photo.farm)", photo.server, photo.id, photo.secret)
        
        return newUrl
    }
    
    func getImageTitle() -> String {
        
        guard let photo = model.photo else {
            return ""
        }
        
        guard let title = photo.title.titleValue else {
            
            var result = ""
            
            if let photoTitle = photo.title.stringValue {
                result = photoTitle
            }
            
            return result
        }
        
        return title._content
    }
    
    func getImageOwner() -> String {
        
        guard let photo = model.photo else {
            return ""
        }
        
        guard let owner = photo.owner.ownerValue else {
            
            var result = ""
            
            if let photoOwner = photo.owner.stringValue {
                result = photoOwner
            }
            
            return result
        }
        
        return owner.username
    }
    
    func getNumberOfDetails() -> Int {
        
        return model.tableViewData.count
    }
    
    func getDetail(at index:Int) -> ImageDetailsTableViewCellModel {
        
        return model.tableViewData[index]
    }
    
    // MARK: - Private Methods
    
    private func createDetailObjects() {
        
        guard let photo = model.photo else {
            return
        }
        
        createAuthorDetailObject()
        createTitleDetailObject()
        createDescriptionDetailObject(photo)
        createTakenDateDetailObject(photo)
        createTagsDetailObject(photo)
    }
    
    private func createAuthorDetailObject() {
        
        model.tableViewData.append(ImageDetailsTableViewCellModel(withTitle: "Author", withText: getImageOwner(), withIconName: "person.circle"))
    }
    
    private func createTitleDetailObject() {
        model.tableViewData.append(ImageDetailsTableViewCellModel(withTitle: "Title", withText: getImageTitle(), withIconName: "textbox"))
    }
    
    private func createDescriptionDetailObject(_ photo: Photo) {
        if let description = photo.description {
            if description._content.count > 0 {
                model.tableViewData.append(ImageDetailsTableViewCellModel(withTitle: "Description", withText: description._content, withIconName: "text.justify"))
            }
        }
    }
    
    private func createTakenDateDetailObject(_ photo: Photo) {
        if let dates = photo.dates {
            model.tableViewData.append(ImageDetailsTableViewCellModel(withTitle: "Taken", withText: dates.taken, withIconName: "clock"))
        }
    }
    
    private func createTagsDetailObject(_ photo: Photo) {
        if let tags = photo.tags, tags.tag.count > 0 {
            var tagArray: [String] = []
            
            for tag in tags.tag {
                tagArray.append(tag.raw)
            }
            
            let stringRepresentation = tagArray.joined(separator: ", ")
            
            model.tableViewData.append(ImageDetailsTableViewCellModel(withTitle: "Tags", withText: stringRepresentation, withIconName: "tag"))
        }
    }
}
