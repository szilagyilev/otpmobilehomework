//
//  DetailsViewCoordinator.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 27..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation
import UIKit

class DetailsViewCoordinator: Coordinator {
    
    // MARK: - Private Properties
    private let presenter: UINavigationController
    private var searchViewController: SearchViewController?
    private var detailsViewController: DetailsViewController?

    private var detailsViewViewModel: DetailsViewViewModel?
    
    // MARK: - Initializers
    init(withPresenter newPresenter: UINavigationController, withPhoto photo: Photo, withApiKey apiKey: String) {
        
        self.presenter = newPresenter
        
        self.detailsViewViewModel = DetailsViewViewModel(withModel: DetailsModel(withPhoto: photo, withApiKey: apiKey), withRemoteService: RemoteServiceImpl())
    }
    
    // MARK: - Public Functions
    func start() {
        
        guard let detailsViewViewModel = detailsViewViewModel else {
            return
        }
        
        let detailsViewController = DetailsViewController.init(withViewModel: detailsViewViewModel)

        presenter.pushViewController(detailsViewController, animated: true)
        self.detailsViewController = detailsViewController
    }
}
