//
//  DetailsModel.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 27..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class DetailsModel {

    var photo: Photo?
    var apiKey: String
    
    var tableViewData: [ImageDetailsTableViewCellModel] = []
    
    let baseImageURLFormat = "https://farm%@.staticflickr.com/%@/%@_%@_b.jpg"
    
    //apiKey, photoId, secret
    let baseImageDetailsURLFormat = "https://www.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=%@&photo_id=%@&secret=%@&format=json&nojsoncallback=1"

    
    init(withPhoto newPhoto:Photo, withApiKey newKey: String) {
        
        self.photo = newPhoto
        self.apiKey = newKey
    }
}
