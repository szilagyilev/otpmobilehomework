//
//  ImageDetailsTableViewCellModel.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 27..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class ImageDetailsTableViewCellModel {
    
    var iconName: String?
    var title: String?
    var text: String?
    
    init(withTitle newTitle: String, withText newText: String, withIconName newIconName: String) {
        
        iconName = newIconName
        title = newTitle
        text = newText
    }
}
