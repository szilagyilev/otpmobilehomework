//
//  AppCoordinator.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 26..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator: Coordinator {
    
    // MARK: - Public Properties
    let window: UIWindow
    let rootViewController: UINavigationController
    
    private var searchViewCoordinator: SearchViewCoordinator
    
    // MARK: - Initializers
    init(window: UIWindow) {
        
        self.window = window
        rootViewController = UINavigationController()
        rootViewController.title = "Root"
        
        searchViewCoordinator = SearchViewCoordinator(withPresenter: rootViewController)
        searchViewCoordinator.start()
    }
    
    // MARK: - Public Functions
    func start() {
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
    }
}
