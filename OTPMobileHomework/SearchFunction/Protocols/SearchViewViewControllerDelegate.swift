//
//  SearchViewViewControllerDelegate.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 26..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

protocol SearchViewViewControllerDelegate: class {
    func selectedImage(_ selectedImage: Photo, withApiKey apiKey: String)
}
