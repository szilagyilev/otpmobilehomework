//
//  SearchModel.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 26..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class SearchModel {
    
    let apiKey = "65803e8f6e4a3982200621cad356be51"
    
    let baseSearchURLFormat = "https://www.flickr.com/services/rest/?method=flickr.photos.search&api_key=%@&tags=%@&per_page=%@&page=%@&format=json&nojsoncallback=1"
    
    let baseImageURLFormat = "https://farm%@.staticflickr.com/%@/%@_%@.jpg"
    
    //https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
    var searchTags: Set = ["dog"]
    var searchText: String = "dog"
    var currentPage = 1
    var itemsPerPage = 20
    var Photos: [Photo] = []
}
