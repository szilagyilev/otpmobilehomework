//
//  SearchViewViewModel.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 26..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class SearchViewViewModel {
    
    // MARK: - Public Properties
    
    // MARK: - Private Properties
    private var model: SearchModel
    private var remoteService: RemoteService
    
    // MARK: - Public Methods
    
    func newSearch(withString searchString: String, completion: @escaping () -> Void) {
        
        resetSearch()
        
        model.searchText = searchString
        createTagArray(fromString: searchString)
        searchForImages(completion: completion)
        
        saveSearchTextToUserDefaults()
    }
    
    func resetSearch() {
        
        model.currentPage = 1
        model.searchTags.removeAll()
        model.searchText = ""
        model.Photos.removeAll()
    }
    
    func loadNextBatchOfResults(completion: @escaping () -> Void) {
        
        createTagArray(fromString: model.searchText)
        model.currentPage += 1
        searchForImages(completion: completion)
    }
    
    func numberOfItems() -> Int {
        
        return model.Photos.count
    }
    
    func item(at index: Int) -> Photo {
        
        return model.Photos[index]
    }
    
    func getURLStringForItem(at index: Int) -> String {
        
        let photo = model.Photos[index]
        let newUrl = String(format: model.baseImageURLFormat, "\(photo.farm)", photo.server, photo.id, photo.secret)
        
        return newUrl
    }
    
    func getApiKey() -> String {
        return model.apiKey
    }
    
    func initSearch(completion: @escaping () -> Void) {
        
        searchForImages(completion: completion)
    }
    
    func loadSearchTextFromUserDefaults() -> String {
        
        var lastSearchText = UserDefaults.standard.string(forKey: "lastSearchText") ?? ""
        
        if lastSearchText.isEmpty {
            lastSearchText = "Dog"
        }
        
        return lastSearchText
    }
    
    // MARK: Initializers
    init(withModel newModel: SearchModel, withRemoteService newRemoteService: RemoteService) {
           
        model = newModel
        remoteService = newRemoteService
    }
    
    // MARK: - Private Methods
    private func saveSearchTextToUserDefaults() {
        UserDefaults.standard.set(model.searchText, forKey: "lastSearchText")
    }
    
    private func createTagArray(fromString searchString: String) {
        
        for newTag in searchString.components(separatedBy: " ") {
            model.searchTags.insert(newTag.lowercased())
        }
    }
    
    private func searchForImages(completion: @escaping () -> Void) {
        
            let newUrl = String(format: model.baseSearchURLFormat, "\(model.apiKey)", getTagsAsURLString().addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!, String(model.itemsPerPage),String(model.currentPage))
            
            remoteService.fetchObjects(urlString: newUrl) {[unowned self] (object: ImageSearchResponse) in

                self.model.Photos.append(contentsOf: object.photos.photo)
                completion()
            }
    }
    
    private func getTagsAsURLString() -> String {
        
        return model.searchTags.joined(separator:"+")
    }
}
