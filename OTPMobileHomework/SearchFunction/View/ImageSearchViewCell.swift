//
//  ImageSearchViewCell.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 27..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation
import Kingfisher
import UIKit

//ViewController for the Image items
class ImageSearchViewCell: UICollectionViewCell {
    
    // MARK: - Private Properties
    // MARK: Layout Elements
    
    private lazy var mainContentView: UIView = {
        let mainContentView = UIView()
        mainContentView.backgroundColor = .clear
        return mainContentView
    }()
    
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.backgroundColor = .clear
        return imageView
    }()
    
    private lazy var activityView: UIActivityIndicatorView = {
        let activityView = UIActivityIndicatorView(style: .medium)
        return activityView
    }()
    
    // MARK: - Public Methods
    // MARK: Initializers
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    // MARK: Other Public Methods
    func showImage(withURLString newURL: String) {
        
        layoutComponents()
        let url = URL(string: newURL)
        imageView.kf.setImage(with: url)
    }
    
    // MARK: - Private Methods
    // MARK: Layouting Methods
    private func layoutComponents() {
        
        contentView.addSubview(mainContentView)
        layoutMainContentView()
        
        mainContentView.addSubview(imageView)
        layoutImageView()
    }
    
    private func layoutImageView() {
        
        imageView.anchor(top: mainContentView.topAnchor,
                         trailing: nil,
                         bottom: mainContentView.bottomAnchor,
                         leading: nil)
        
        imageView.centerXAnchor.constraint(equalTo: mainContentView.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: mainContentView.centerYAnchor).isActive = true
    }
    
    private func layoutMainContentView() {
        
        contentView.clipsToBounds = true
        
        mainContentView.anchor(top: contentView.topAnchor,
                               trailing: contentView.trailingAnchor,
                               bottom: contentView.bottomAnchor,
                               leading: contentView.leadingAnchor)
    }
}
