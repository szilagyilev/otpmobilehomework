//
//  ViewController.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 26..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    // MARK: - Public Properties
    weak var delegate: SearchViewViewControllerDelegate?
    
    // MARK: - Private Properties
    private var viewModel: SearchViewViewModel?
    
    private let viewTitle = "Flickr Image Search"
    
    let searchController = UISearchController(searchResultsController: nil)
    
    private var cellIdentifier = "imageSearchItemCell"

    // MARK: Layout Elements
    private let activityView = UIActivityIndicatorView(style: .medium)
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.register(ImageSearchViewCell.self, forCellWithReuseIdentifier: cellIdentifier)
        collectionView.backgroundColor = .systemGray6
        
        if let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.scrollDirection = .vertical
            flowLayout.minimumLineSpacing = 10
        }
        
        return collectionView
    }()
    
    // MARK: - Public Methods
    
    // MARK: Initializers
    init(withViewModel newViewModel: SearchViewViewModel) {
        
        super.init(nibName: nil, bundle: nil)
        
        viewModel = newViewModel
        collectionView.dataSource = self
        collectionView.delegate = self
    }
       
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupViewController()
        
        setupViewAppearance()
        
        layoutComponents()
        
    }
    
    // MARK: - Private Methods
    // MARK: Layouting Methods
    
    private func layoutComponents() {
        
        view.addSubview(collectionView)
        layoutCollectionView()
        
        view.addSubview(activityView)
        layoutActivityView()
    }
    
    private func layoutActivityView() {
        
        activityView.anchor(top: nil,
                            trailing: collectionView.trailingAnchor,
                            bottom: collectionView.bottomAnchor,
                            leading: collectionView.leadingAnchor)
    }
    
    private func layoutCollectionView() {
        
        collectionView.anchor(top: view.safeAreaLayoutGuide.topAnchor,
                              trailing: view.safeAreaLayoutGuide.trailingAnchor,
                              bottom: view.safeAreaLayoutGuide.bottomAnchor,
                              leading: view.safeAreaLayoutGuide.leadingAnchor,
                              padding: UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12))
    }
    
    // MARK: Setup Methods
    
    private func setupViewController() {
        
        navigationItem.searchController = searchController
        
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        
        initSearch()
    }
    
    private func setupViewAppearance() {
        
        navigationController?.navigationBar.prefersLargeTitles = false
        
        view.backgroundColor = .systemGray6
        title = viewTitle
        
        let logo = UIImage(named: "findr_logo.png")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        
        searchController.searchBar.placeholder = "Search Flickr for Images"
    }
    
    private func initSearch() {
        
        guard let viewModel = viewModel else {
            return
        }
        
        guard var searchExpression = searchController.searchBar.text else {
            return
        }
        
        if !(searchExpression.count > 0) {
           searchExpression = viewModel.loadSearchTextFromUserDefaults()
        }
        
        self.activityView.startAnimating()
        viewModel.newSearch(withString: searchExpression) {
        DispatchQueue.main.async {[unowned self] in
                self.activityView.stopAnimating()
                self.collectionView.reloadData()
            self.searchController.isActive = false
            }
        }
    }
}

// MARK: - Extensions
extension SearchViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {

    }
    
}

extension SearchViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        initSearch()
    }
}

extension SearchViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        guard let viewModel = viewModel else {
            return 0
        }
        
        return viewModel.numberOfItems()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let viewModel = viewModel else {
            return UICollectionViewCell()
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ImageSearchViewCell
        
        cell.showImage(withURLString: viewModel.getURLStringForItem(at: indexPath.row))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let viewModel = viewModel else {
            return
        }
        
        if (indexPath.row == viewModel.numberOfItems() - 1 ) {
            
            self.activityView.startAnimating()
            viewModel.loadNextBatchOfResults() {
                DispatchQueue.main.async {[unowned self] in
                    self.activityView.stopAnimating()
                    self.collectionView.reloadData()
                }
            }
        }
    }
}

extension SearchViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        let size = (collectionView.frame.width-40)/2
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        guard let viewModel = viewModel else {
            return
        }
        
        delegate?.selectedImage(viewModel.item(at: indexPath.row), withApiKey: viewModel.getApiKey())
    }
}

extension SearchViewController: UICollectionViewDelegate {
    
}
