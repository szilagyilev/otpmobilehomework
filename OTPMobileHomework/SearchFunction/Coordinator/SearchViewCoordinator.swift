//
//  SearchViewCoordinator.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 26..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import CoreLocation
import UIKit

class SearchViewCoordinator: Coordinator {
    
    // MARK: - Private Properties
    private let presenter: UINavigationController
    private var searchViewController: SearchViewController?
    private var detailsViewCoordinator: DetailsViewCoordinator?
    
    // MARK: - Initializers
    init(withPresenter newPresenter: UINavigationController) {
        self.presenter = newPresenter
    }
    
    // MARK: - Public Functions
    func start() {
        
        let searchViewController = SearchViewController.init(withViewModel: SearchViewViewModel(withModel: SearchModel(), withRemoteService: RemoteServiceImpl()))

        searchViewController.delegate = self

        presenter.pushViewController(searchViewController, animated: true)
        
        self.searchViewController = searchViewController
    }
}

// MARK: - Extensions
extension SearchViewCoordinator: SearchViewViewControllerDelegate {
    func selectedImage(_ selectedImage: Photo, withApiKey apiKey: String) {
        
        let detailsViewCoordinator = DetailsViewCoordinator(withPresenter: presenter, withPhoto: selectedImage, withApiKey: apiKey)
        
        detailsViewCoordinator.start()
        self.detailsViewCoordinator = detailsViewCoordinator
    }
}
