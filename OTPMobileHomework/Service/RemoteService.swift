//
//  RemoteService.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 26..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

protocol RemoteService {
    func fetchObjects<T: Decodable>(urlString: String, completion: @escaping(T) -> ())
}
