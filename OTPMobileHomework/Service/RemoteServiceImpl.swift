//
//  RemoteServiceImpl.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 26..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class RemoteServiceImpl: RemoteService {
    
    func fetchObjects<T: Decodable>(urlString: String, completion: @escaping(T) -> ()) {
        if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url) { (data, resp, err) in
                guard let data = data else {
                    return
                }
                
                do {
                    let object = try JSONDecoder().decode(T.self, from: data)
                    completion(object)
                } catch let jsonError {
                    print("Failed to read feed: \(jsonError)")
                }
                }.resume()
        }
    }
}
