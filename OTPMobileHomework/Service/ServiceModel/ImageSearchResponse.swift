//
//  ImageSearchResponse.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 26..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class ImageSearchResponse: Codable {
    
    var photos: Photos
}

//{
//   "photos":{
//      "page":1,
//      "pages":1,
//      "perpage":20,
//      "total":16,
//      "photo":[
//         {
//            "id":"48820663261",
//            "owner":"88469486@N04",
//            "secret":"899b4083c7",
//            "server":"65535",
//            "farm":66,
//            "title":"Suse",
//            "ispublic":1,
//            "isfriend":0,
//            "isfamily":0
//         },
//         {
//            "id":"48752588928",
//            "owner":"55046645@N00",
//            "secret":"c316ffa7ac",
//            "server":"65535",
//            "farm":66,
//            "title":"Hometown wanderings",
//            "ispublic":1,
//            "isfriend":0,
//            "isfamily":0
//         }
//      ]
//   },
//   "stat":"ok"
//}
