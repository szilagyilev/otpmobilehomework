//
//  Photos.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 26..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class Photos: Codable {
    
    var page: Int
    var pages: Int
    var perpage: Int
    var total: String
    
    var photo: [Photo]
}
