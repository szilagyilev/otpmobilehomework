//
//  Photo.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 26..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class Photo: Codable {
    
    var id: String
    var owner: VarValue
    var secret: String
    var server: String
    var farm: Int
    var title: VarValue
    
    var description: Description?
    var dates: Dates?
    var tags: Tags?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case owner = "owner"
        case secret = "secret"
        case server = "server"
        case farm = "farm"
        case title = "title"
        case description = "description"
        case dates = "dates"
        case tags = "tags"
    }
}

enum VarValue: Codable {
    case string(String)
    case ownerItem(Owner)
    case titleItem(Title)

    init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if let x = try? container.decode(String.self) {
            self = .string(x)
            return
        }
        if let x = try? container.decode(Owner.self) {
            self = .ownerItem(x)
            return
        }
        if let x = try? container.decode(Title.self) {
            self = .titleItem(x)
            return
        }
        throw DecodingError.typeMismatch(VarValue.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for VarValue"))
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        switch self {
        case .string(let x):
            try container.encode(x)
        case .ownerItem(let x):
            try container.encode(x)
        case .titleItem(let x):
            try container.encode(x)
        }
    }
    
    var stringValue: String? {
        switch self {
        case .string(let s):
            return s
        default:
            return nil
        }
    }
    
    var titleValue: Title? {
        switch self {
        case .titleItem(let s):
            return s
        default:
            return nil
        }
    }
    
    var ownerValue: Owner? {
        switch self {
        case .ownerItem(let s):
            return s
        default:
            return nil
        }
    }
}
//            "id":"48820663261",
//            "owner":"88469486@N04",
//            "secret":"899b4083c7",
//            "server":"65535",
//            "farm":66,
//            "title":"Suse",
//            "ispublic":1,
//            "isfriend":0,
//            "isfamily":0
