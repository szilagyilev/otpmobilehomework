//
//  Owner.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 27..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class Owner: Codable {
    
    var username: String
    var realname: String
    var location: String
}


//"owner": {
//"nsid": "160226510@N02",
//"username": "Pawel Mikulski",
//"realname": "Paweł Mikulski",
//"location": "Krakow",
//"iconserver": "65535",
//"iconfarm": 66,
//"path_alias": null
//}
