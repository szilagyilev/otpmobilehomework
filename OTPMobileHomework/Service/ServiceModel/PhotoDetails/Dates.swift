//
//  Dates.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 27..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class Dates: Codable {
    
    var posted: String
    var taken: String
}


//"dates": {
//"posted": "1571748470",
//"taken": "2019-09-27 08:05:53",
//"takengranularity": "0",
//"takenunknown": "0",
//"lastupdate": "1571883767"
//},
