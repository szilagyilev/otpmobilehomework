//
//  Tag.swift
//  OTPMobileHomework
//
//  Created by Levente Szilágyi on 2019. 10. 27..
//  Copyright © 2019. szilagyilev. All rights reserved.
//

import Foundation

class Tag: Codable {
    
    var id: String
    var raw: String
}


//"id": "160206162-48941581106-231236",
//"author": "160226510@N02",
//"authorname": "Pawel Mikulski",
//"raw": "tarty",
//"_content": "tarty",
//"machine_tag": 0
